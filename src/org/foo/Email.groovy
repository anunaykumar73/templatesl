package org.foo;
import jenkins.model.*
import hudson.model.*
	//java.lang.String Jobname1, email1;
	def sendEmail(def Jobname1, def email1){
			def jenkinsInstance = jenkins.model.Jenkins.getInstance()
			def job = jenkinsInstance.getItemByFullName(Jobname1)
			def lastBuild = job.lastBuild.result
                        emailext body: "${lastBuild}: Job ${Jobname1} \n				                                                  		 	Last build status: ${lastBuild}",				                                                                             		subject: "Jenkins Build ${lastBuild}: Job ${Jobname1}",				                                                        		to: email1
	}